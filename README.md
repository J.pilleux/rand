Themisator
----------

A simple random choice program in CLI ¯\_(ツ)_/¯

Install from sources
--------------------

I am kinda too lazy to find out how to create a release with a binary soooo, you will have to compile it manually with dune.
Dune is a OCaml build system, you can find all information to install and use it [here](https://github.com/ocaml/dune)

    git clone https://gitlab.com/J.pilleux/rand
    cd rand
    dune build

If the command succeed, you will find the binary here: `_build/default/bin/main.exe`
You can then move it into your PATH if you want

Run with Dune
-------------

If you prefer you can run it directly with Dune:

    dune exec rand -- parameter1 parameter2 ...

Run the shlagito binary
-----------------------

If you feel lucky, you can try running the binary I push at the root of the project.
I know this is gross, but I am THIS lazy to learn how to do it in release page hehe.
The binary is built with :

- NixOS channel stable 23.11
- dune 3.11.1
- opam 2.1.5

Hope it will work for you ¯\_(ツ)_/¯

Usage
-----

It is dead simple. Just invoke the program with whatever parameter you want to randomize

    rand choice1 choice2 choice3
