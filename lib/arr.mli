(** Returns the same array but without the first element *)
val remove_first: string array -> string array

(** Returns a random element from the input array *)
val extract_rand: string array -> string

(** Joins an array with a given separator *)
val join: string array -> string -> string
