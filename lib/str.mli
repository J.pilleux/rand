(** Pretty print the result of the choice *)
val format_choice: string array -> string -> string
