type color = Blue | Yellow;;

let decorate col = 
    match col with
        | Blue -> Printf.sprintf "\027[34m%s\027[0m"
        | Yellow -> Printf.sprintf "\027[33m%s\027[0m"

let repeat_string str_to_repeat times =
  let rec repeat_helper acc remaining =
    if remaining <= 0 then
      acc
    else
      repeat_helper (acc ^ str_to_repeat) (remaining - 1)
  in
  repeat_helper "" times

let box str =
    let len = String.length str in
    let hor = repeat_string "─" (len + 4) in
    let s = [
        "\t┌" ^ hor ^ "┐";
        Printf.sprintf "\t│  %s  │" (decorate Blue str);
        "\t└" ^ hor ^ "┘\n";
    ] in
    String.concat "\n" s

let format_choice choices pick = 
    let str_choices = Arr.join choices ", " in
    let dashes = decorate Yellow "---" in
    let 
    s = [
        dashes ^ (decorate Blue " Random choice ") ^ dashes;
        Printf.sprintf "\n  Available choices where: [ %s ]" (decorate Blue str_choices);
        "  The selected choice is:\n";
        box pick;
    ] 
    in
    String.concat "\n" s;
