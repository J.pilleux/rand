let join arr sep = String.concat sep (Array.to_list arr)

let remove_first arr = Array.sub arr 1 (Array.length arr - 1) 

let rand_num max =
    Random.self_init();
    Random.int (max)

let extract_rand arr = 
    let rng = rand_num (Array.length arr) in
    arr.(rng)

