{ pkgs ? import <nixpkgs> {} }:

  pkgs.mkShell {
    nativeBuildInputs = with pkgs.buildPackages; [ dune_3 ocaml opam ];

    script = ''
      eval $(opam env --switch=default)
    '';
}

