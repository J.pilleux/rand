open Rando

let usage = "Usage: rand choice1 choice2 choice3"

let () = 
    let arr = Arr.remove_first Sys.argv in
    match arr with
        | [| |] -> print_endline usage
        | arr -> print_endline (Str.format_choice arr (Arr.extract_rand arr))
