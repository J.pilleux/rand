install_path="$$HOME/.local/mybin/rand"
bin=./_build/default/bin/main.exe

all: build

build:
	dune build

run:
	dune exec rand -- test1 test2 test3

reinstall: uninstall install

install: $(bin)
	cp $< $(install_path)

uninstall:
	rm $(install_path)
